from django.db import models


# Create your models here.
class Book(models.Model):
    author = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    year = models.IntegerField()

    def __str__(self):
        return self.name
